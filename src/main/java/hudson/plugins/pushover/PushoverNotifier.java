package hudson.plugins.pushover;

import hudson.Extension;
import hudson.Launcher;
import hudson.model.BuildListener;
import hudson.model.Result;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.Hudson;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Notifier;
import hudson.tasks.Publisher;

import java.io.IOException;

import net.pushover.client.PushoverException;
import net.pushover.client.PushoverMessage;
import net.pushover.client.PushoverRestClient;
import net.pushover.client.Status;
import net.sf.json.JSONObject;

import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.StaplerRequest;
import org.springframework.security.util.TextUtils;

/**
 * Hudson notifier for Pushover.net service.
 * 
 * @author Neil Gall
 */
public class PushoverNotifier extends Notifier {

	public final String apiKey;
	public final String userToken;
	public final boolean notifySuccess;

	@DataBoundConstructor
	public PushoverNotifier(String apiKey, String userToken, boolean notifySuccess) {
		this.apiKey = apiKey;
		this.userToken = userToken;
		this.notifySuccess = notifySuccess;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hudson.tasks.BuildStep#getRequiredMonitorService()
	 */
	public BuildStepMonitor getRequiredMonitorService() {
		return BuildStepMonitor.NONE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hudson.tasks.BuildStepCompatibilityLayer#perform(hudson.model.AbstractBuild
	 * , hudson.Launcher, hudson.model.BuildListener)
	 */
	@Override
	public boolean perform(AbstractBuild<?, ?> build, Launcher launcher,
			BuildListener listener) throws InterruptedException, IOException {
		
		if (!notifySuccess && build.getResult() == Result.SUCCESS) {
			return true;
		}
		
		final PushoverMessage.Builder builder = PushoverMessage
				.builderWithApiToken(apiKey);
		builder.setUserId(userToken);
		builder.setTitle(build.getProject().getName());
		builder.setMessage(getResultMessage(build.getResult(),
				build.getDurationString()));

		final String rootUrl = Hudson.getInstance().getRootUrl();
		final String buildUrl = (rootUrl == null) ? null : rootUrl.concat(build
				.getUrl());
		builder.setUrl(buildUrl);

		try {
			final PushoverRestClient client = new PushoverRestClient();
			final Status status = client.pushMessage(builder.build());
			listener.getLogger().printf("Pushover: " + status + "\n");
			return true;

		} catch (PushoverException ex) {
			listener.getLogger().printf(
					"Pushover failed: " + ex.toString() + "\n");
			return false;
		}
	}

	private String getResultMessage(final Result result,
			final String durationString) {
		final StringBuilder sb = new StringBuilder();
		if (result == Result.ABORTED) {
			sb.append("Aborted");
		} else if (result == Result.FAILURE) {
			sb.append("FAILED");
		} else if (result == Result.NOT_BUILT) {
			sb.append("Not built");
		} else if (result == Result.SUCCESS) {
			sb.append("Success");
		} else if (result == Result.UNSTABLE) {
			sb.append("Unstable");
		}
		if (durationString != null && durationString.length() > 0) {
			sb.append(" after ").append(durationString);
		}
		return sb.toString();
	}

	@Extension
	public static final class DescriptorImpl extends
			BuildStepDescriptor<Publisher> {

		private static final String API_KEY_TAG = "pushoverApiKey";
		private static final String USER_TOKEN_TAG = "userToken";
		private static final String NOTIFY_SUCCESS_TAG = "notifySuccess";

		private String apiKey;

		public DescriptorImpl() {
			load();
		}

		public String getApiKey() {
			return apiKey;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see hudson.tasks.BuildStepDescriptor#isApplicable(java.lang.Class)
		 */
		@Override
		public boolean isApplicable(Class<? extends AbstractProject> jobType) {
			return true;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see hudson.model.Descriptor#getDisplayName()
		 */
		@Override
		public String getDisplayName() {
			return "Pushover Notifications";
		}

		@Override
		public boolean configure(StaplerRequest req, JSONObject json)
				throws FormException {
			apiKey = req.getParameter(API_KEY_TAG);
			save();
			return super.configure(req, json);
		}

		@Override
		public Publisher newInstance(StaplerRequest req, JSONObject json)
				throws FormException {
			final String userToken = json.getString(USER_TOKEN_TAG);
			final boolean notifySuccess = json.getBoolean(NOTIFY_SUCCESS_TAG);
			return new PushoverNotifier(apiKey, userToken, notifySuccess);
		}

	}
}
